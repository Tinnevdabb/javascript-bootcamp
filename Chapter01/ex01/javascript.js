var names = [
    "Tinne",
    "Anouck",
    "Roel",
    "Andy",
    "Peppi",
    "Kokki",
    "Thomas",
    "Wim",
    "Tom",
    "Elise",
    "Ilano",
    "Rune",
    "Sanne",
    "Koen"
];

function newName() {
    var random = Math.floor(Math.random() * names.length);
    document.getElementById("nameDisplay").innerHTML = names[random];
}

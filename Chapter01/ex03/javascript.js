const url = encodeURIComponent(document.getElementById("searchTerm").value);

function search() {
  if (document.searchForm.searchEngine.value === "google") {
    window.open("http://www.google.com/search?q=" + url);
  } else if (document.searchForm.searchEngine.value === "bing") {
    window.open("http://www.bing.com/search?q=" + url);
  } else {
    window.open("https://duckduckgo.com/?q=" + url);
  }
}
